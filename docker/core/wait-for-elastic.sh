#!/bin/sh
# wait-for-elastic.sh

set -e

host="$1"
shift
cmd="$@"

until curl "$host"; do
    >&2 echo "elastic is unavailable - sleeping"
    sleep 1
done

>&2 echo "elastic is ready"
exec $cmd