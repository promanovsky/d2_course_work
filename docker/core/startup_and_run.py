from http.server import BaseHTTPRequestHandler
import socketserver
import json
import cgi

from haystack import Finder
from haystack.indexing.cleaning import clean_wiki_text
from haystack.indexing.io import write_documents_to_db
from haystack.reader.farm import FARMReader
from haystack.database.elasticsearch import ElasticsearchDocumentStore
from haystack.retriever.elasticsearch import ElasticsearchRetriever
from elasticsearch import Elasticsearch
from pathlib import Path
import logging

logger = logging.getLogger(__name__)

# docker container run -d -p 9200:9200 -e "discovery.type=single-node" --name=elastic_search elasticsearch:7.6.1
# docker container run --name d2_core_run -p 7575:7575 --link elastic_search -v ${PWD}:/usr/src/app  d2_core

doc_dir = 'all_documents'
model_dir = 'saved_model'
# elastic_host = 'localhost'
elastic_host = 'elastic_search'
elastic_port = ':9200'
port = 7575
host = 'd2_core'
# host = 'localhost'

def write_documents_to_db_utf8(document_store, document_dir, clean_func=None, only_empty_db=False,
                               split_paragraphs=False):
    file_paths = Path(document_dir).glob("**/*.txt")
    if only_empty_db:
        n_docs = document_store.get_document_count()
        if n_docs > 0:
            logger.info(f"Skip writing documents since DB already contains {n_docs} docs ...  "
                        "(Disable `only_empty_db`, if you want to add docs anyway.)")
            return None

    docs_to_index = []
    doc_id = 1
    for path in file_paths:
        with open(path, encoding="utf8") as doc:
            text = doc.read()
            if clean_func:
                text = clean_func(text)

            if split_paragraphs:
                for para in text.split("\n\n"):
                    if not para.strip():  # skip empty paragraphs
                        continue
                    docs_to_index.append(
                        {
                            "name": path.name,
                            "text": para
                        }
                    )
                    doc_id += 1
            else:
                docs_to_index.append(
                    {
                        "name": path.name,
                        "text": text
                    }
                )
    document_store.write_documents(docs_to_index)
    logger.info(f"Wrote {len(docs_to_index)} docs to DB")

print('haystack initialization start')
document_store = ElasticsearchDocumentStore(host=elastic_host, username="", password="", index="document")
write_documents_to_db_utf8(document_store=document_store, document_dir=doc_dir, clean_func=None,
                           only_empty_db=False, split_paragraphs=True)

# write_documents_to_db(document_store=document_store, document_dir=doc_dir, clean_func=None,
#                           only_empty_db=True, split_paragraphs=True)
retriever = ElasticsearchRetriever(document_store=document_store)

# reader = FARMReader(model_name_or_path=model_dir, use_gpu=False, num_processes=0) # localhost version
reader = FARMReader(model_name_or_path=model_dir, use_gpu=False, num_processes=4) # compose

finder = Finder(reader, retriever)
elastic_client = Elasticsearch(hosts=[elastic_host + elastic_port])
print('haystack initialization complete')


class RequestHandler(BaseHTTPRequestHandler):
    def _set_headers(self):
        self.send_response(200)
        self.send_header('Content-type', 'application/json')
        self.end_headers()

    def do_OPTIONS(self):
        self.send_response(200, "ok")
        self.send_header('Access-Control-Allow-Origin', '*')
        self.send_header('Access-Control-Allow-Methods', 'GET, POST')
        self.send_header("Access-Control-Allow-Headers", "X-Requested-With, Content-Type")
        self.send_header('Content-type', 'application/json')

    def do_POST(self):
        ctype, pdict = cgi.parse_header(self.headers.get_content_type())

        if ctype != 'application/json':
            self.send_response(400)
            self.end_headers()
            return

        req = json.loads(self.rfile.read(int(self.headers['Content-Length'])))
        q = req['question']
        if len(q) > 0:

            top_k_retriever = int(req['retriever_top_size'])
            top_k_reader = int(req['reader_size'])

            prediction = finder.get_answers(q, top_k_reader=top_k_reader, top_k_retriever=top_k_retriever)
            top_answer = ''
            document_name = ''
            document_text = ''
            answers = []

            if len(prediction['answers']) > 0:
                top_answer = prediction['answers'][0]['answer']
                query_body = {
                    "query": {
                        "match": {
                            "text": top_answer
                        }
                    }
                }
                results = elastic_client.search(index="document", body=query_body)
                if len(results['hits']['hits']) > 0:
                    document_name = results['hits']['hits'][0]['_source']['name']
                    document_text = results['hits']['hits'][0]['_source']['text']
                for i in range(top_k_reader):
                    answers.append(prediction['answers'][i]['answer'])

            answer = {
                'question': q,
                'top_answer': top_answer,
                'answers': answers,
                'document_name': document_name,
                'document_text': document_text
            }
            self._set_headers()
            self.wfile.write(bytes(json.dumps(answer), encoding='utf-8'))
        else:
            self._set_headers()
            self.wfile.write(bytes(json.dumps({"error": "empty question"}), encoding='utf-8'))


handler = RequestHandler
with socketserver.TCPServer((host, port), handler) as httpd:
    httpd.serve_forever()
