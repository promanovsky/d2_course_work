import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {RequestDto} from '../model/request-dto.model';
import {ResponseDto} from '../model/response-dto.model';

@Injectable({
  providedIn: 'root'
})
export class DataTransferService{

  private askUrl = '/api';

  constructor(private httpClient: HttpClient) {
  }

  askServer = (request: RequestDto): Observable<ResponseDto> => {
    return this.httpClient.post<ResponseDto>(this.askUrl, request, {
      observe: 'body',
      responseType: 'json'
    });
  };

}
