export class ResponseDto {
  question: string;
  top_answer: string;
  answers: string[];
  document_name: string;
  document_text: string;
}
