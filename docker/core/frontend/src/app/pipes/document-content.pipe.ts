import {Pipe, PipeTransform} from '@angular/core';
import {isIterable} from 'rxjs/internal-compatibility';

@Pipe({
  name: 'documentContent'
})
export class DocumentContentPipe implements PipeTransform {

  private regExp = /\[IMG_[0-9]*\]/g;

  transform(value: string, ...args: unknown[]): string {
    const img_arr = value.match(this.regExp);
    if(img_arr && isIterable(img_arr)) {
      for (let v of img_arr) {
        if (v) {
          value = value.replace(v, '<img src=\'assets/img/' +
            v.replace('[', '').replace(']', '') +
            '.png\' alt=\'\'>');
        }
      }
    }
    return value.replace(/\n/g, '<br/>');
  }

}
