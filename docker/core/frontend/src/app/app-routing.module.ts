import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {CdqaFilterComponent} from './components/cdqa-filter/cdqa-filter.component';

export const routes: Routes = [
  {
    path: '',
    component: CdqaFilterComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(
    routes
  )],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
