import { Component, OnInit } from '@angular/core';
import {DataTransferService} from '../../service/data-transfer.service';
import {RequestDto} from '../../model/request-dto.model';
import {Observable, of} from 'rxjs';
import {ResponseDto} from '../../model/response-dto.model';

@Component({
  selector: 'app-cdqa-filter',
  templateUrl: './cdqa-filter.component.html',
  styleUrls: ['./cdqa-filter.component.css']
})
export class CdqaFilterComponent implements OnInit {

  readerSize:number = 1;
  retrieverSize:number = 5;
  question: string = 'Запрос на русском';
  result: Observable<ResponseDto>;
  loading = false;

  constructor(private service: DataTransferService) { }

  ngOnInit(): void {
  }

  click(){
    let req = new RequestDto();
    req.question = this.question || ' ';
    req.retriever_top_size = this.retrieverSize;
    req.reader_size = this.readerSize;
    this.loading = true;
    this.service.askServer(req).subscribe(resp=>{
      this.result = of(resp);
      this.loading = false;
    });
  }

}
