import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CdqaFilterComponent } from './cdqa-filter.component';

describe('CdqaFilterComponent', () => {
  let component: CdqaFilterComponent;
  let fixture: ComponentFixture<CdqaFilterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CdqaFilterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CdqaFilterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
