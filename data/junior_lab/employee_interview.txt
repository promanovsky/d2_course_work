Представитель Посмотреть список собеседований
[IMG_20]
Чтобы посмотреть список собеседований Джуниоров или Кандидатов, выберите раздел "Собеседование". В данном списке отображаются список собеседований за определенный месяц.
Для каждого собеседования отображается следующая информация:
Дата - для запросов на собеседования и отклоненных запросов отображается дата запроса, для остальных собеседований отображается запланированная дата проведения собеседования.
Сотрудник - Джуниор или Кандидат, которого будут собеседовать
Офис - офис Джуниора или Кандидита, с возможностью отфильтровать по офису
Технология - технология Джуниора или Кандидита
Время - для собеседований (кроме запросов на собеседования и отклоненных запросов) отображается запланированное время проведения и длительность собеседования.
при нажатии - переход на карточку собеседования.

Представитель Посмотреть карточку собеседования
Представитель Посмотреть информацию о собеседовании
[IMG_21]
Для запрошенного или отклоненного собеседования показывается период, который указал Джуниор, для остальных статусов собеседования - дата и время собеседования.
Если допустимая длительность (1 месяц работы на каждые 30 минут собеседования) меньше, чем длительность собеседования, показывается предупреждение.

Представитель Посмотреть информацию по вопросах
[IMG_25]
Когда собеседование окончено в информации по вопросам, отображаются оценки и комментарии собеседующих, а также заключение по вопросу Руководителю JL.

Представитель Поставить оценку
[IMG_46]
Чтобы подвести поставить оценку Джуниору, откройте карточку собеседования в статусе "Ожидание оценок". Выберите опцию и поставьте оценку, если выбрали опцию "Оценить вопрос", и напишите комментарий. Нажмите "Подтвердить отзыв".
