Представитель Посмотреть список собеседующих         
Чтобы посмотреть список собеседующих, в разделе "Справочники" выберите справочник "Собеседующие". В данном списке отображаются все пользователи с ролью "Собеседующий".
Список собеседующих можно отфильтровать по офису и технологию, найти собеседующего, введя в поисковую строку над таблицей фамилию и/или имя пользователя на русском языке.
При нажатии на фамилию и имя собеседующего, открывается карточка данного пользователя. (см. Посмотреть карточку пользователя)

