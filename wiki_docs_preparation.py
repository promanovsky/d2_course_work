import os
import re
from os import listdir
from os.path import isfile, join
from docx import Document
import win32com.client as win32
from win32com.client import constants

docs_path = 'wiki_docs'
output_path = os.path.join('wiki_docs', 'out')

documents = [f for f in listdir(docs_path) if isfile(join(docs_path, f))]

ABS_PATH = os.path.abspath(docs_path)


def save_as_docx(path) -> str:
    word = win32.gencache.EnsureDispatch('Word.Application')
    path = os.path.join(ABS_PATH, path)
    doc = word.Documents.Open(path)
    doc.Activate()
    new_file_abs = re.sub(r'\.\w+$', '.docx', path)
    word.ActiveDocument.SaveAs(
        new_file_abs, FileFormat=constants.wdFormatXMLDocument
    )
    doc.Close(False)
    return new_file_abs


def parse_doc_to_txt(path, doc_number, min_para_value=300):
    path = os.path.abspath(path)
    doc = Document(path)
    txt_doc = ''
    line = ''
    for p in doc.paragraphs:
        line = line + ' ' + p.text
        if len(line) < min_para_value:
            continue
        else:
            txt_doc = txt_doc + line + '\n'
            line = ''
    url = os.path.join(output_path, 'doc_' + str(doc_number) + '.txt')
    with open(url, encoding='utf-8', mode='w+') as f:
        f.write(txt_doc)


if not os.path.exists(output_path):
    os.makedirs(output_path)


for num, d in enumerate(documents):
    if d.endswith('.doc'):
        d = save_as_docx(d)
        parse_doc_to_txt(d, num)
    elif d.endswith('.docx'):
        parse_doc_to_txt(os.path.join(docs_path, d), num)
